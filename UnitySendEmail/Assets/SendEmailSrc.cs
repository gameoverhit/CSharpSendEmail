﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class SendEmailSrc : MonoBehaviour
{
	void OnGUI()
	{
		if (GUI.Button(new Rect(0, 50, 100, 40), "Capture"))
		{
			Debug.Log("Capture Screenshot");
			Application.CaptureScreenshot("screen.png");
		}
		if (GUI.Button(new Rect(0, 0, 100, 40), "Send"))
		{
			SendEmail();
		}
	}

	private void SendEmail()
	{
		MailMessage mail = new MailMessage();

		mail.From = new MailAddress("1213250243@qq.com");
		mail.To.Add("1213250243@qq.com");
		mail.Subject = "Test Mail";
		mail.Body = "This is for testing SMTP mail from GMAIL";
		mail.Attachments.Add(new Attachment("screen.png"));

		SmtpClient smtpServer = new SmtpClient("smtp.qq.com");
		smtpServer.Credentials = new System.Net.NetworkCredential("1213250243@qq.com", "密码") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback =
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{ return true; };

		smtpServer.Send(mail);
		Debug.Log("success");
	}
}